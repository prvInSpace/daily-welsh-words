package cymru.prv.daily;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.security.auth.login.LoginException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;

public class DailyWelsh extends TimerTask {

    Timer timer = new Timer();
    LocalDate lastDay = LocalDate.ofEpochDay(0);
    List<Word> words = new LinkedList<>();
    List<String> subscribers = new LinkedList<>();
    JDA api;
    int index;

    public DailyWelsh() throws IOException, LoginException, InterruptedException {
        loadWords();

        if (Files.exists(Path.of("cache.json"))) {
            JSONObject cache = new JSONObject(Files.readString(Path.of("cache.json")));
            index = cache.optInt("index", 0);
            lastDay = LocalDate.ofEpochDay(cache.getLong("lastDay"));
        }

        if (Files.exists(Path.of("subscribers.json"))) {
            JSONArray subscribersArray = new JSONArray(Files.readString(Path.of("subscribers.json")));
            for (int i = 0; i < subscribersArray.length(); i++)
                subscribers.add(subscribersArray.getString(i));
        }

        api = JDABuilder.createDefault("ADD KEY")
                .addEventListeners(new DiscordListener(this))
                .build();
        api.awaitReady();

        System.out.println(getStatus());

        timer.scheduleAtFixedRate(this, 0, 60000);
        saveSubscribers();
    }

    public void loadWords() throws IOException {
        JSONArray wordsArray = new JSONArray(Files.readString(Path.of("words.json")));
        List<Word> newWords = new LinkedList<>();
        for (int i = 0; i < wordsArray.length(); i++) {
            newWords.add(new Word(wordsArray.getJSONObject(i)));
        }

        System.out.println("Number of words: " + newWords.size());
        words = newWords;
    }

    public void unsubscribe(String channel) {
        subscribers.remove(channel);
        saveSubscribers();
    }

    public void subscribe(String channel) {
        if (!subscribers.contains(channel)) {
            subscribers.add(channel);
            saveSubscribers();
        }
    }

    public String getStatus(){
        String format = "%-17s : %d\n";
        String sb = "```\n" +
                String.format(format, "Number of words", words.size()) +
                String.format(format, "Current index", index) +
                String.format(format, "Subscribers", subscribers.size()) +
                String.format("%-20s%s", "Last day", lastDay.toString()) +
                "```";
        return sb;
    }

    @Override
    public void run() {
        if (LocalDate.now().isAfter(lastDay))
            update();
    }

    public void update(){
        index++;
        if (index >= words.size())
            index = 0;
        sendDailyWords();
    }

    public void sendDailyWords() {
        String dailyWord = getDailyWord();
        for (String s : subscribers) {
            TextChannel channel = api.getTextChannelById(s);
            if (channel == null)
                continue;
            channel.sendMessage(dailyWord).queue();
        }

        lastDay = LocalDate.now();

        JSONObject obj = new JSONObject().put("lastDay", lastDay.toEpochDay()).put("index", index);
        try (PrintWriter out = new PrintWriter("cache.json")) {
            out.println(obj.toString());
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getDailyWord(){
        Word word = words.get(index < words.size() ? index : 0);
        StringBuilder sb = new StringBuilder();
        sb.append("The Welsh word, sentence or phrase of the day is: *").append(word.getCy()).append("*\n");
        sb.append("This translates to: *").append(word.getEn()).append("*\n").append("\n");
        sb.append("If you got any suggestions, feedback, or wishes please don't hesitate to contact Prv. If you want help with conjugations, mutations, and spell checking please check out the bot Digi!").append("\n");
        return sb.toString();
    }

    private void saveSubscribers() {
        JSONArray array = new JSONArray(subscribers);
        try (PrintWriter out = new PrintWriter("subscribers.json")) {
            out.println(array.toString());
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            new DailyWelsh();
        } catch (IOException | LoginException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
