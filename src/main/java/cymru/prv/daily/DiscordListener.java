package cymru.prv.daily;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.io.IOException;

public class DiscordListener extends ListenerAdapter {

    private DailyWelsh daily;

    public DiscordListener(DailyWelsh daily) {
        this.daily = daily;
    }

    @Override
    public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent event) {
        if (!event.getAuthor().getId().equals("184407866906640384"))
            return;
        String message = event.getMessage().getContentStripped();
        if (message.contains("reload")) {
            System.out.print("Reloading words: ");
            try {
                daily.loadWords();
                System.out.println("complete");
            } catch (IOException e) {
                System.out.println("failed");
                e.printStackTrace();
            }
        } else if (message.contains("forcerun")) {
            daily.update();
            System.out.println("Sending out daily word");
        } else if (message.contains("status"))
            event.getChannel().sendMessage(daily.getStatus()).queue();

    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (event.getAuthor().isBot())
            return;
        if (event.isWebhookMessage())
            return;
        if (event.getMember() == null)
            return;

        String message = event.getMessage().getContentStripped();
        if (event.isFromGuild()) {
            if (event.getMessage().getMentionedMembers().contains(event.getGuild().getSelfMember())) {
                if (event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
                    if (message.contains("unsubscribe")) {
                        daily.unsubscribe(event.getChannel().getId());
                        event.getChannel().sendMessage("Channel unsubscribed").queue();
                    } else if (message.contains("subscribe")) {
                        daily.subscribe(event.getChannel().getId());
                        event.getChannel().sendMessage("Channel is now subscribed").queue();
                    } else if (message.contains("fetch")) {
                        event.getChannel().sendMessage(daily.getDailyWord()).queue();
                    }
                }
                else {
                    event.getChannel().sendMessage("Only administrators are allowed to do that.").queue();
                }
            }
        }
    }
}
