package cymru.prv.daily;

import org.json.JSONObject;

public class Word {

    private String cy;
    private String en;

    public Word(JSONObject obj){
        cy = obj.getString("cy");
        en = obj.getString("en");
    }

    public String getCy() {
        return cy;
    }

    public String getEn() {
        return en;
    }
}
